require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest
	test "list" do
		for i in 1..5
			ProductsHelper.build("List00#{i}","Create #{i}", "Create desc #{i}", i, 29.90, {i: i})
		end
		get '/products'
		resp = JSON.parse(@response.body)
		
		assert resp["products"].size == 5
		for i in 0..4
			assert resp["products"][i]["code"] , "List00#{i+1}"
		end	

		#and now the attrs 
		for i in 1..5
			params = {search: {attrs: "i #{i}"}}
			get '/products', params: params
			resp = JSON.parse(@response.body)
			assert resp["products"].size == 1
			assert resp["products"].first["attrs"]["i"] == i
		end
	end

	test "Show" do
		prod = ProductsHelper.build("Show001", "Show 001", "Show z z o", 10, 15.9, nil)
		get "/products/#{prod.id}"
		resp = JSON.parse(@response.body)

		assert resp["status"] == "Success"
		assert resp["product"]["id"] == prod.id

		#and fails
		get "/products/#{prod.id + 1}"
		resp = JSON.parse(@response.body)
		assert resp["status"] == "Error"
	end

	test "Create True" do
		params = {code: 'Create001', name: "Create 1", desc: "Create zero zero one", in_stock: 10, price: 20.00, attrs: {"thename" => "theone"}}
		post '/products', params: params
		prods = Product.where(code: "Create001")

		assert prods.size == 1
		assert prods.first.code == params[:code]
	end

	test "Create False" do
		params = {code: 'Create001', name: nil, desc: "Create zero zero one", in_stock: 10, price: 20.00, attrs: {"thename" => "theone"}}
		post '/products', params: params
		prods = Product.where(code: "Create001")

		assert prods.size == 0
		assert prods.first == nil
	end

	test "Update" do
		prod = ProductsHelper.build("Update001", "Update 001", "Update z z o", 10, 15.9, nil)
		put "/products/#{prod.id}", params: {name: "New Update 001"}
		new_prod = Product.find(prod.id)
		assert new_prod.name == "New Update 001"
		assert prod.code == new_prod.code
	end

	test "Update fails" do
		prod1 = ProductsHelper.build("Update001", "Update 001", "Update z z o", 10, 15.9, nil)
		put "/products/#{prod1.id + 1}", params: {name: "I should fail"}
		
		resp = JSON.parse(@response.body)
		assert resp["status"] == "Error"
		
		prod1.reload
		assert prod1.name == "Update 001"
	end

	test "Delete" do
		p_id = ProductsHelper.build("Delete", "Delete 001", "Delete z z o", 10, 15.9, nil).id
		delete "/products/#{p_id}"
		notfound = false
		Product.find(p_id) rescue notfound = true
		assert notfound == true
	end

end
