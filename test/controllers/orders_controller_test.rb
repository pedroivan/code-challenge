require 'test_helper'

class OrdersControllerTest < ActionDispatch::IntegrationTest
	test "Order Index" do
		generateExample()
		get '/orders'
		resp = JSON.parse(@response.body)

		assert resp["orders"].size == 1
		order = resp["orders"].first
		assert order["client"] == "Jão"
		assert order["state"] == "Nova"
		assert order["products"].size == 2
		assert order["products"][0]["code"] == "prod1"
		assert order["products"][1]["code"] == "prod2"
	end

	test "Create" do
		generateExample()

		products = []
		Product.all.each do |prod|
			products << {
				id: prod.id,
				quantity: 10,
				price: prod.id
			}
		end
		params = {
			products: products,
			code: "Venda Create",
			client_id: Client.first.id,
			order_state_id: OrderState.first.id,
			freight: 10.50,
			date: '2018-12-25'
		}

		post "/orders", params: params
		order = Order.where(code: "Venda Create")

		assert order.size == 1
		order = order.first
		assert order.client.name == "Jão"
		assert order.order_state.name == "Nova"
		assert order.products.size == 2
		assert Product.where(code: 'prod1')[0].in_stock == 5
		assert Product.where(code: 'prod2')[0].in_stock == 10
	end

	test "Create Fail" do 
		generateExample

		product = Product.where(code: "prod1").first
		old_stock = product.in_stock

		params = {
			products: {
				id: product.id,
				quantity: old_stock + 1,
				price: product.price
				},
			code: "Venda Create",
			client_id: Client.first.id,
			order_state_id: OrderState.first.id,
			freight: 10.50,
			date: '2018-12-25'
		}

		post "/orders", params: params
		product.reload
		assert product.in_stock == old_stock
		assert Order.where(code: "Venda Create").size == 0
		assert OrderProduct.all.joins(:order).where("orders.code = 'Venda Create'").size == 0
	end

	test "Show" do
		generateExample()
		order = Order.first
		get "/orders/#{order.id}"
		resp = JSON.parse(@response.body)

		assert resp["status"] == "Success"
		assert resp["order"]["id"] == order.id

		#and fails
		get "/orders/#{order.id + 1}"
		resp = JSON.parse(@response.body)
		assert resp["status"] == "Error"
	end

	test "Update" do
		generateExample()

		order = Order.first
		new_code = "Update New Order #{order.id}"
		prod_ids = order.products.map{|p| p.id}
		products = prod_ids.map{|p| {id: p} }

		params = {
			code: new_code,
			client_id: Client.first.id,
			order_state_id: OrderState.first.id,
			products: products
		}

		put "/orders/#{order.id}", params: params

		assert order.reload.code == new_code
		assert order.products.size == 2

		#testando remover 1 item
		product_id = products[0][:id]
		
		params[:products] = [{
			id: product_id,
			quantity: 1,
			price: 1.99
		}]

		put "/orders/#{order.id}", params: params
		assert order.products.size == 1
		assert order.products.first.id == product_id

		#agora adiciona 1 item
		new_product_id = products[1][:id]

		params[:products] << {
			id: new_product_id,
			quantity: 1,
			price: 5.00
		}	

		put "/orders/#{order.id}", params: params

		order.reload
		assert order.products.size == 2

		#agora muda 1 item
		target_op = OrderProduct.where(order_id: order.id, product_id: params[:products][0][:id]).first
		old_qtt = target_op.product.in_stock
		params[:products][0][:quantity] = 5 #de 1 foi para 5
		
		put "/orders/#{order.id}", params: params

		assert old_qtt - 4 == target_op.reload.product.in_stock

	end

	private
	def generateExample
		OrdersHelper.chain_build(
			'ordercode', #order.code
			Date.today(), #order.date
			{name: "Jão"}, #client.name (pode ser id)
			{name: "Nova"}, #order_states.name (pode ser id)
			12, #order.freight
			[
				{
					product: #pode ser product: product.id também
						{code: 'prod1', #product.code
							name: 'name1', #product.name
							price: 12, #product.price
							in_stock: 15
						}, 
					quantity: 10, #order_products.quantity
					price: 12.90 #order_products.price
				},
				{
					product: #pode ser product: product.id também
						{code: 'prod2', #product.code
							name: 'name2', #product.name
							price: 15, #product.price
							in_stock: 30
						}, 
					quantity: 20, #order_products.quantity
					price: 20.00 #order_products.price
				}
			]
		)
	end
end
