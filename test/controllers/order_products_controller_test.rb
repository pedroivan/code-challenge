require 'test_helper'

class OrderProductsControllerTest < ActionDispatch::IntegrationTest
	test "the truth" do #tinha que ter um né
		generate()
		params = {initial_date: '2019-01-01', final_date: '2019-12-31'}
		get "/average_ticket" , params: params
		
		resp = JSON.parse @response.body
		assert resp["ticket"]["total_sum"] == 305
		assert resp["ticket"]["total_clients"] == 2
		assert resp["ticket"]["result"] == 152.5

		params = {initial_date: '2019-01-01', final_date: '2019-01-01'}

		get "/average_ticket" , params: params
		
		resp = JSON.parse @response.body
		assert resp["ticket"]["total_sum"] == 180
		assert resp["ticket"]["total_clients"] == 1
		assert resp["ticket"]["result"] == 180

	end

	private
	def generate()
		client_ids = [
			ClientsHelper.build("Cliente 1", nil, nil).id,
			ClientsHelper.build("Cliente 2", nil, nil).id,
			ClientsHelper.build("Cliente 3", nil, nil).id
		]

		prod_ids = [
			ProductsHelper.build("code1", "name 1", "desc 1", 50, 20, nil).id,
			ProductsHelper.build("code2", "name 2", "desc 2", 30, 20, nil).id,
			ProductsHelper.build("code3", "name 3", "desc 3", 30, 10, nil).id
		]

		order_states_id = [
			OrderStatesHelper.build("nova").id,
			OrderStatesHelper.build("entregue").id
		]

		orders = [
			OrdersHelper.build("Client 1 State 1", "2019-01-01", client_ids[0], order_states_id[1], 0).id,
			OrdersHelper.build("Client 1 State 2", "2019-01-01", client_ids[0], order_states_id[0], 0).id,
			OrdersHelper.build("Client 2 State 1 Date +10", "2019-01-11", client_ids[1], order_states_id[1], 0).id
		]

		#order 1: 50 + 10 + 120 = 180
		OrderProductsHelper.build(orders[0], prod_ids[0], 5, 10) 
		OrderProductsHelper.build(orders[0], prod_ids[1], 2, 5) 
		OrderProductsHelper.build(orders[0], prod_ids[2], 10, 12) 

		#order 2 (n conta): 100 + 25 + 5 = 130
		OrderProductsHelper.build(orders[1], prod_ids[0], 10, 10) 
		OrderProductsHelper.build(orders[1], prod_ids[1], 5, 5) 
		OrderProductsHelper.build(orders[1], prod_ids[2], 1, 5) 

		#order 3 : 100 + 25 = 125
		OrderProductsHelper.build(orders[2], prod_ids[0], 10, 10) 
		OrderProductsHelper.build(orders[2], prod_ids[1], 5, 5) 
	end
end
