require 'test_helper'

class ProductTest < ActiveSupport::TestCase
	test "I'm Ok" do
		product = ProductsHelper.build('Prod001', 'Produto 1', 'Produto número um (hum)', 1, 11.0, {'position': 'first'})
		assert product.id != nil
		assert product.code, 'Prod001'
	end

	test "Not Nulls" do

		mandatories = [:code, :name, :price]
		values = ['Code', 'Name', 1]
		for i in 0...mandatories.size
			couldnot = false
			copy = values
			copy[i] = nil
			ProductsHelper.build(copy[0], copy[1], nil, nil, copy[3], nil) rescue couldnot = true
			assert couldnot
		end
	end

	test "Code Unicity" do
		p1 = ProductsHelper.build('Prod001', 'Produto 1', 'Produto número um (hum)', 1, 11.0, {'position': 'first'})
		p2 = ProductsHelper.build('Prod002', 'Produto 2', 'Produto número dois', 2, 12.0, {'position': 'almost there'})
		p1.update(code: p2.code) rescue nil
		
		p1.reload
		assert p1.code != p2.code
	end

	private 

end
