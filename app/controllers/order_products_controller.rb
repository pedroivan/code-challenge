class OrderProductsController < ApplicationController
	def average_ticket
		begin 
			raise "Precisa preencher datas" if params["initial_date"] == nil or params["final_date"] == nil
			resp = OrderProduct.average_ticket(params["initial_date"], params["final_date"])
			return render json: {status: "Success", ticket: resp}
		rescue => e
			return render json: {status: "Error", message: e.message}
		end
	end
end
