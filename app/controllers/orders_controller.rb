class OrdersController < ApplicationController

	def index
		resp = []
		code = params["search"] ? params["search"]["code"] : nil
		search = Order.index(code)

		#orders = search.select("orders.code, orders.date, orders.client_id, clients.name, orders.order_state_id, order_states.name, orders.freight, order_products.*, products.code")
		search.select("orders.*,clients.name, order_states.name").distinct.each do |order|
			products = []
			order.order_products.each do |op|
				products <<	{
						code: op.product.code,
						quantity: op.quantity,
						price: op.price
					}
			end
			resp << {
				code: order.code,
				date: order.date,
				state: order.order_state.name,
				client: order.client.name,
				freight: order.freight,
				products: products
			}
		end

		return render json: {status: "Success", orders: resp}
	end

	def show
		begin
			order = Order.find(params["id"])
			return render json: {status: "Success", order: order}
		rescue => e
			return render json: {status: "Error", message: e.message}
		end
	end

	def create
		begin
			raise "Venda sem produtos" if params["products"].size == 0

			attrs = ApplicationHelper.checkAttributes(params, [:code, :client_id, :order_state_id] , [:date, :freight] )
			new_guy = Order.create(attrs)
			
			params["products"].each do |product|
				OrderProduct.create(
						order_id: new_guy.id, 
						product_id: product["id"], 
						quantity: product["quantity"], 
						price: product["price"]
				)
			end
			
			return render json: {status: "Success"}
		rescue => e
			return render json: {status: "Error", message: e.message}
		end
	end

	def update
		begin
			raise "Venda sem produtos" if params["products"] == nil or params["products"].size == 0

			attrs = ApplicationHelper.checkAttributes(params, [:code, :client_id, :order_state_id] , [:date, :freight] )		
			Order.find(params["id"]).update(attrs)

			#faz uma lista de remoção, uma de adição e uma para os iguais (possível update)
			new_ids = params["products"].map{|p| p["id"].to_i}
			old_ids = OrderProduct.where(order_id: params["id"]).all.map{|p| p.product_id}
			
			remove = old_ids - new_ids

			if remove.size > 0
				ops = OrderProduct.where("order_id = #{params["id"]} and product_id in (?)", remove.join(","))
				ops.each do |op|
					Product.find(op.product_id).increase(op.quantity)
					op.destroy
				end
			end

			add = new_ids - old_ids
			
			if add.size > 0
				params["products"].each do |p|
					next if add.include?(p["id"].to_i) == false
					OrderProductsHelper.build(params["id"], p["id"], p["quantity"].to_i, p["price"])
				end
			end

			commom = new_ids & old_ids
			params["products"].each do |p|
				if commom.include?(p["id"].to_i)
					attrs = ApplicationHelper.attributesUpdate(OrderProduct, p)
					if attrs.size > 0
						op = OrderProduct.where(order_id: params["id"], product_id: p["id"]).first
						old_stock = op.quantity
						op.update(attrs)
						new_stock = attrs["quantity"].to_i
						if old_stock != new_stock
							Product.find(p["id"]).decrease(new_stock - old_stock)
						end
					end
				end
			end

		rescue => e
			pp e.message
			pp e.backtrace
			return render json: {status: "Error", message: e.message}
		end
	end

end



