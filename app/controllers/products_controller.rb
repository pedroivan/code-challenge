class ProductsController < ApplicationController
	def index

		begin
			search = Product.all

			if params['search']
				if params['search']['code']
					search = search.where(code: params['search']['code'])
				end
			
				if params['search']['attrs']
					split = params['search']['attrs'].split(" ")
					key = split[0]
					value = split[1..-1].join("%")
					search = search.where(" attrs->> '#{key}' like '%#{value}%'")
				end
			end

			search = search.order(:code)
			return render json: {status: "Success", products: search.all}

		rescue => error
			return render json: {status: "Error", message: e.message}
		end
	end

	def show
		begin
			product = Product.find(params["id"])
			return render json: {status: "Success", product: product}
		rescue => e
			return render json: {status: "Error", message: e.message}
		end
	end

	def create
		begin
			attrs = ApplicationHelper.checkAttributes(params, [:code, :name, :price], [:desc, :in_stock, :attrs])
			new_guy = Product.create(attrs)
			redirect_to action: "show", id: new_guy.id
		rescue => e
			return render json: {status: "Error", message: e.message}
		end
	end

	def update 
		begin
			product = Product.update(params["id"], ApplicationHelper.attributesUpdate(Product, params))
			redirect_to action: "show", id: params["id"]
		rescue => e
			return render json: {status: "Error", message: e.message}
		end
	end

	def destroy
		begin
			OrderProduct.where(product_id: params["id"]).destroy_all
			Product.delete(params["id"])
			return render json: {status: "Success"}
		rescue => e
			return render json: {status: "Error", message: e.message}
		end
	end
end
