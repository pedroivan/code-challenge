module ClientsHelper
	def self.build(name, contact_1, contact_2)
		Client.create(name: name, contact_1: contact_1, contact_2: contact_2)
	end
end