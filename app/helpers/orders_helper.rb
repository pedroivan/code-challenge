module OrdersHelper
	#cria apenas a ordem (chaves estrangeiras existem)
	def self.build(code, date, client_id, order_state_id, freight)
		Order.create(code: code, 
			date: date, 
			client_id: client_id, 
			order_state_id: order_state_id, 
			freight: freight
			)
	end

	#Auxiliar para testes
	#aceita um hash para ser criado, ou o id:
	#- client
	#- order_state
	#- cada elemento de order_products, sendo order_product[:product]
	def self.chain_build(code, date, client, order_state, freight, order_products)
		if client.instance_of? Hash
			client = ClientsHelper.build(client[:name], client[:contact_1], client[:contact_2])
			client = client.id
		end

		if order_state.instance_of? Hash
			order_state = OrderStatesHelper.build(order_state[:name])
			order_state = order_state.id
		end

		order_products.each do |op|
			product = op[:product]
			if product.instance_of? Hash
				product = ProductsHelper.build(product[:code], product[:name], product[:desc], product[:in_stock], product[:price], product[:attrs])
				product = product.id
			end
			op[:product] = product
		end

		order_id = self.build(code, date, client, order_state, freight).id

		order_products.each do |op|
			OrderProductsHelper.build(order_id, op[:product], op[:quantity], op[:price])
		end
		
	end
end
