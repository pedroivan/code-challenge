module OrderStatesHelper
	def self.build(name)
		OrderState.create(name: name)
	end
end