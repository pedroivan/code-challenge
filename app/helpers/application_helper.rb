module ApplicationHelper
	def self.checkAttributes(params, mandatories, opts)
		resp = {}
		mandatories.each do |m|
			if params[m]
				resp[m] = params[m]
			else
				raise "Precisa preencher #{m}"
			end
		end
		opts.each do |m|
			if params[m]
				resp[m] = params[m]
			end
		end
		return resp
	end

	def self.attributesUpdate(model, params)
		names = model.attribute_names
		resp = {}
		names.each do |name|
			if params[name] != nil and ["id","created_at","updated_at"].include?(name) == false
				resp[name] = params[name]
			end
		end
		return resp
	end
end
