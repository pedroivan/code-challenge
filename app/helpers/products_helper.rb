module ProductsHelper
	def self.build(code, name, desc, in_stock, price, attrs)
		Product.create({code: code, name: name, desc: desc, in_stock: in_stock, price: price, attrs: attrs})
	end
end
