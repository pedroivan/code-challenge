module OrderProductsHelper
	def self.build(order_id, product_id, quantity, price)
		product = Product.find(product_id)
		raise "Não existem produtos o suficiente para #{product_id}" if product.canDecrease(quantity.to_i) == false
		
		OrderProduct.create(
			order_id: order_id, 
			product_id: product_id, 
			quantity: quantity, 
			price: price).save
		product.decrease(quantity)
	end
end