class Product < ApplicationRecord
	#in_stock > 0
	#price > 0
	has_many :order_products
	has_many :orders, through: :order_products

	def canDecrease(quantity)
		return self.in_stock >= quantity
	end

	def decrease(quantity)
		self.in_stock -= quantity
		self.save
	end

	def increase(quantity)
		decrease(quantity * -1)
	end
end
