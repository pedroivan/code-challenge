class OrderProduct < ApplicationRecord
	belongs_to :order
	belongs_to :product

	def self.average_ticket(i_date, f_date)
		resp = ActiveRecord::Base.connection.execute(ActiveRecord::Base.sanitize_sql("
			select 
				sum(OP.quantity * OP.price) as total_sum,
				count(distinct O.client_id) as total_clients,
				count(distinct O.id) as total_orders,
				sum(OP.quantity * OP.price) / count(distinct O.client_id) as result
			from order_products OP
			inner join orders O
				on O.id = OP.order_id
			inner join order_states OS
				on OS.id = O.order_state_id
			where O.date >= '#{i_date}' and O.date <= '#{f_date}'
				and OS.name = 'entregue'"))

		resp.map{|r| r}.first

	end
end
