class Order < ApplicationRecord
	belongs_to :client
	belongs_to :order_state

	has_many :order_products
	has_many :products, through: :order_products

	def self.index(code = nil)
		search = Order
		search = Order.where(code: code) if code

		self.joins(:client).
			joins(:order_state).
			joins(:order_products).
			joins(:products)
	end

end