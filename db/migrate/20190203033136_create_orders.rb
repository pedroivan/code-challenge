class CreateOrders < ActiveRecord::Migration[5.2]
	def change
		create_table :orders do |t|
			t.string :code, null: false, unique: true
			t.date :date, default: -> {'CURRENT_TIMESTAMP'}
			t.references :client, null: false
			t.references :order_state, null: false, default: 1
			t.float :freight, default: 0
			t.timestamps
		end
	end
end
