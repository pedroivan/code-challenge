class CreateClients < ActiveRecord::Migration[5.2]
	def change
		create_table :clients do |t|
			t.string :name, null: false
			t.string :contact_1
			t.string :contact_2
			t.timestamps
		end
	end
end
