class CreateProducts < ActiveRecord::Migration[5.2]
	def change
		create_table :products do |t|
			t.string :code, null: false
			t.string :name, null: false
			t.string :desc
			t.integer :in_stock, default: 0, null: false
			t.float :price, null: false
			t.json :attrs
			t.timestamps
		end
		add_index :products, :code, unique: true
	end
end
