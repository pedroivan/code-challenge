class CreateOrderProducts < ActiveRecord::Migration[5.2]
	def change
		create_table :order_products do |t|
			t.integer :order_id, null: false
			t.integer :product_id, null: false
			t.integer :quantity, null: false
			t.float :price, null: false
			t.index [:order_id, :product_id], unique: true
		end
		add_foreign_key :order_products, :orders
		add_foreign_key :order_products, :products
	end
end
