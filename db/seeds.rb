
#o default em Order.order_state_id é 1
OrderState.create([
	{name: "novo"},
	{name: "aprovado"},
	{name: "entregue"},
	{name: "cancelado"}
])

Client.create([
	{name: "Fulaninha de Tal", contact_1: "laninha_tal@gmail.com"},
	{name: "Joaquim, é claro", contact_1: "Vizinho", contact_2: "De frente"}
])

Product.create([
	{code: "Codigo001", name: "Produto 1", desc: "O primeiro de muitos", price: 10.00},
	{code: "Codigo002", name: "Produto 2", desc: "Segundo, mas importante", price: 9.90, attrs: {tamanho: "15.5 x 13.0", peso: "0.9kg"}},
	{code: "Codigo003", name: "Produto 3", desc: "Balinhas sortidas", price: 12.90, attrs: {menta: true, anis: "claro que não", chocolate: 2}}
])

Order.create([
	{code: "Venda001", date: Date.parse("2019-01-03"), client_id: 1, order_state_id: 3, freight: 30.00},
	{code: "Venda002", date: Date.parse("2019-01-17"), client_id: 1, order_state_id: 2, freight: 40.00},
	{code: "Venda003", client_id: 2, order_state_id: 4, freight: 0.00},
	{code: "Venda004", client_id: 1}
])

OrderProduct.create([
	{order_id: 1, product_id: 1, quantity: 7, price: 10},
	{order_id: 1, product_id: 2, quantity: 14, price: 9.9},
	{order_id: 1, product_id: 3, quantity: 3, price: 12.5},

	{order_id: 2, product_id: 2, quantity: 3, price: 9.9},
	{order_id: 2, product_id: 3, quantity: 5, price: 12.9},

	{order_id: 3, product_id: 1, quantity: 10, price: 10},
	{order_id: 3, product_id: 2, quantity: 5, price: 9.9},

	{order_id: 4, product_id: 3, quantity: 42, price: 12}
])