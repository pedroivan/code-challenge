Decisões tomadas estão argumentadas em decisions.txt

Ruby 2.5.1
Postgresql 9.5.14

#Crie seu gemset (se preferir)
rvm gemset create pedroivan
rvm 2.5.1@pedroivan
(ps: Não sou narcisista, o nome é apenas para distinguir, ok? :) )

#Bundle
gem install bundler
bundle install

#Criando banco
rake db:create
rake db:migrate
rake db:seed

#Executando
rails s (para aplicação)
rails test (para os testes)

##Interfaces

#Products
Index: Get "/products" 
params: {
	search: { (opt)
		code: string, 
		attrs: string (attrs para acessar o json, primeira palavra chave, restante, valor)
	} 

Show: Get "/products/:id" sem params

Create: Post "/products" 
params: {
	code: string, 
	name: string, 
	desc: string, (opt)
	in_stock: 10, (opt, default 0)
	price: 20.00, 
	attrs: (opt) {
		chave => valor,
		chave2 => valor2
	}

Update: Put "/products/:id"
{
	code: string, 
	name: string, 
	desc: string, (opt)
	in_stock: 10, (opt, default 0)
	price: 20.00, 
	attrs: (opt) {
		chave => valor,
		chave2 => valor2
}

Destroy: Delete "/products/:id" sem params

#Orders

Index: Get "/orders" 
params: {
	search: { (opt)
		code: string
	}
}

Show: Get "/orders/:id" sem params

Create: Post "/orders"
params:{
	code: string,
	client_id: integer,
	order_state_id: integer,
	date: date, (opt, default today())
	freight: float (opt)
	products: [
		{
			id: integer,
			quantity: integer, 
			price: float
		},
		... 
	]
}

Update: Put "/orders/:id"
Nota: Update verifica a diferença entre o estado do banco e o do update para criar/remover/atualizar
	portanto é importante passar todos os produtos, mesmo sem alteração
params: {
	code: string,
	client_id: integer,
	order_state_id: integer,
	freight: float, (opt)
	date: date, (opt, default today())
	products: [
		{
			id: integer,
			quantity: integer, 
			price: float
		},
		... 
	]	
}

#Average Ticket
Get "/average_ticket"
params: {
	initial_date: date,
	final_date: date
}

Qualquer dúvida me contatem pelo email pedroivanalima@gmail.com