Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :products
  resources :orders
  get 'average_ticket', to: "order_products#average_ticket"
end
